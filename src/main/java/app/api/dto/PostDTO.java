package app.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PostDTO {
    private String postText;
    private String userId;
    private String hashtag;
    private int likes;
    private int dislikes;
}
