package app.api;

import app.api.dto.PostDTO;
import app.api.dto.UserDTO;
import app.api.error.ServiceException;
import app.api.mapper.ApiDtoMapper;
import app.service.UserService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;
    protected final ApiDtoMapper mapper;

    @PostMapping()
    public Mono<UserDTO> createUser(@RequestBody UserDTO user) {
        return userService.createUser(user.getUserName(), user.getGender())
                .map(mapper::userEntityToUserDto);
    }

    @GetMapping
    public Flux<UserDTO> findAllUsers(){
        return userService.getAllUsers()
                .map(mapper::userEntityToUserDto);
    }

    @GetMapping("{userId}/posts")
    public Flux<PostDTO> findAllUsersPosts(@PathVariable("userId") String userId){
          return Mono.just(UUID.fromString(userId))
                  .flatMapMany(userService::getUsersPosts)
                  .map(mapper::postEntityToPostDto);
    }
}
