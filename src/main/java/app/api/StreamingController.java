package app.api;

import java.time.Duration;
import java.util.stream.IntStream;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class StreamingController {

    @GetMapping(value = "/api/test/stream",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Integer> stream(){
        return Flux.fromStream(IntStream.rangeClosed(1,30).boxed()).delayElements(Duration.ofSeconds(1));
    }
}
