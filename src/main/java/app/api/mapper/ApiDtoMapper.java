package app.api.mapper;

import app.api.dto.PostsDTO;
import app.api.dto.PostDTO;
import app.api.dto.TagDTO;
import app.api.dto.UserDTO;
import app.model.Tags;
import app.model.User;
import java.util.UUID;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ApiDtoMapper {
    UserDTO userEntityToUserDto(User user);
    @Mapping(source = "userId", target = "userId", qualifiedByName = "uuidToString")
    PostDTO postEntityToPostDto(app.model.Posts posts);

    @Mapping(source = "userId", target = "userId", qualifiedByName = "uuidToString")
    app.model.Posts postDtoToPostEntity(PostDTO postDTO);
    @Mapping(source = "userId", target = "userId", qualifiedByName = "uuidToString")
    app.model.Posts postAggToPostEntity(PostsDTO post);

    @Mapping(source = "taggedTo", target = "taggedTo", qualifiedByName = "uuidToString")
    @Mapping(source = "postsId", target = "postsId", qualifiedByName = "uuidToString")
    TagDTO tagsToTagDto(Tags tags);

    @Named("uuidToString")
    default String uuidToString(UUID item){
        return item.toString();
    }

    @Named("uuidToString")
    default UUID stringToUuid(String item){
        return UUID.fromString(item);
    }
}
