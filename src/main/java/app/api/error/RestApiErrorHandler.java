package app.api.error;

import io.r2dbc.spi.R2dbcException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@RestControllerAdvice
@Slf4j
public class RestApiErrorHandler {

    @ExceptionHandler(DataAccessException.class )
    Mono<ResponseEntity<String>> genericDbError(DataAccessException ex) {
        log.error("handling exception g::" + ex.getMessage());
        log.error("handling exception g::" + ex.getClass().getName());
        return Mono.just(ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(ex.getMessage()));
    }

    @ExceptionHandler(R2dbcException.class )
    Mono<ResponseEntity<String>> r2dbcException(R2dbcException ex) {
        log.error("handling exception r::" + ex.getMessage());
        log.error("handling exception r::" + ex.getClass().getName());
        return Mono.just(ResponseEntity.status (500).contentType(MediaType.APPLICATION_JSON).body(ex.getMessage()));
    }
    @ExceptionHandler(ServiceException.class )
    Mono<ResponseEntity<String>> serviceException(ServiceException ex) {
        log.error("handling exception r::" + ex.getMessage());
        log.error("handling exception r::" + ex.getClass().getName());
        return Mono.just(ResponseEntity.status (404).contentType(MediaType.APPLICATION_JSON).body(ex.getMessage()));
    }
}
