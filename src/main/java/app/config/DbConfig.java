package app.config;

import io.r2dbc.pool.ConnectionPool;
import io.r2dbc.pool.ConnectionPoolConfiguration;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import java.time.Duration;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

import static io.r2dbc.spi.ConnectionFactoryOptions.DATABASE;
import static io.r2dbc.spi.ConnectionFactoryOptions.DRIVER;
import static io.r2dbc.spi.ConnectionFactoryOptions.HOST;
import static io.r2dbc.spi.ConnectionFactoryOptions.PASSWORD;
import static io.r2dbc.spi.ConnectionFactoryOptions.PORT;
import static io.r2dbc.spi.ConnectionFactoryOptions.PROTOCOL;
import static io.r2dbc.spi.ConnectionFactoryOptions.USER;


//@Configuration
public class DbConfig { //extends AbstractR2dbcConfiguration {

   // @Override
  //  @Bean
  //  public ConnectionFactory connectionFactory() {
       /* ConnectionFactory connectionFactory = ConnectionFactories.get(ConnectionFactoryOptions.builder()
                .option(DRIVER, "pool")
                .option(PROTOCOL, "postgresql") // driver identifier, PROTOCOL is delegated as DRIVER by the pool.
                .option(HOST, "127.0.0.1")
                .option(PORT, 5432)
                .option(USER, "javauser")
                .option(PASSWORD, "Password123")
                .option(DATABASE, "reactive")
                .build());
        ConnectionPoolConfiguration configuration = ConnectionPoolConfiguration.builder(connectionFactory)
                .maxIdleTime(Duration.ofMillis(21000))
                .maxSize(3)
                .maxLifeTime(Duration.ofMillis(1800000))
             //   .metricsRecorder(DatabaseMetricsRecorder())
                .build();

        return new ConnectionPool(configuration);*/
  //  }

  /*  @Override
    protected List<Object> getCustomConverters() {
        return List.of(new UUIDToByteArrayConverter(), new ByteArrayToUUIDConverter());
    }*/
}
