package app.model.base;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

@Getter
@Setter
public abstract class BaseUUIDEntity {
    @Id
    private UUID id;
    private LocalDateTime createdDt;
    private LocalDateTime updatedDt;
    private String createdBy;
    private String updatedBy;
    @Version
    private int version;
}
