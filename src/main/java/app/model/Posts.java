package app.model;

import app.model.base.BaseUUIDEntity;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
@Table(schema = "post",name = "posts")
public class Posts extends BaseUUIDEntity {
    private String postText;
    private UUID userId;
    private String hashtag;
    private int likes;
    private int dislikes;

}
