package app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PostVO {
    private String postText;
    private String userId;
    private String hashtag;
    private int likes;
    private int dislikes;

    private String taggedTo;
}
