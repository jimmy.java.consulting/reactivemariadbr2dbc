package app.service;

import app.api.dto.PostsDTO;
import app.api.error.ServiceException;
import app.model.Tags;
import app.model.User;
import app.model.base.BaseUUIDEntity;
import app.repository.PostsRepository;
import app.repository.TagsRepository;
import app.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PostsRepository postsRepository;

    private final TagsRepository tagsRepository;

    @Transactional
    public Mono<User> createUser(String userName,String gender){
        return userRepository.findByUserName(userName)
                .flatMap(user -> Mono.error(new IllegalArgumentException("user already present")))
                .switchIfEmpty(userRepository.save(User.builder().userName(userName).gender(gender).build()))
                .cast(User.class);
    }

    public Flux<User> getAllUsers(){
        return userRepository.findAll();
    }

    public Mono<Boolean> userExists(String userId) {
        return getUUIDFromString(userId)
                .flatMap(userRepository::existsById);
    }

    public Mono<User> findById(UUID userId) {
        return userRepository.findById(userId);
    }

    @Transactional
    public Mono<app.model.Posts> createPosts(String text, UUID userId){
        return postsRepository.save(app.model.Posts.builder().postText(text).userId(userId).build());
    }

    @Transactional
    public Mono<app.model.Posts> createPosts(app.model.Posts posts){
        return postsRepository.save(posts);
    }

    @Transactional
    public Mono<Void> updateLikesByOne(UUID postid){
        return postsRepository.findById(postid)
                .flatMap(post -> {
                    post.setLikes(post.getLikes()+1);
                    return Mono.just(post);
                })
                .flatMap(postsRepository::save).then();
    }

    @Transactional
    public Mono<Void> updateLikesByOne(String hashtag){
        return postsRepository.findByHashtag(hashtag)
                .flatMap(post -> {
                    post.setLikes(post.getLikes()+1);
                    return Mono.just(post);
                })
                .flatMap(postsRepository::save).then();
    }

    @Transactional
    public Mono<Void> updateDislikesByOne(String hashtag){
        return postsRepository.findByHashtag(hashtag)
                .flatMap(post -> {
                    post.setDislikes(post.getDislikes()+1);
                    return Mono.just(post);
                })
                .flatMap(postsRepository::save).then();
    }

    @Transactional
    public Mono<app.model.Posts> createPostsWithSingleTag(String text, UUID userId, UUID taggedTo){
        return postsRepository.save(app.model.Posts.builder().postText(text).userId(userId).build())
                .flatMap(posts -> tagsRepository.save(Tags.builder().postsId(posts.getId()).taggedTo(taggedTo).build()).thenReturn(posts));
    }

    @Transactional
    public Mono<Void> createPostWithTags(String text,UUID userId,UUID... tags) {
          return postsRepository.save(app.model.Posts.builder().postText(text).userId(userId).build())
                   .map(BaseUUIDEntity::getId)
                   .flatMapMany(id -> getAllTags(id,tags))
                   .flatMap(tagsRepository::save).then();
    }

    @Transactional
    public Mono<Void> createPostWithTags(app.model.Posts post, UUID... tags) {
        return postsRepository.save(post)
                .map(BaseUUIDEntity::getId)
                .flatMapMany(id -> getAllTags(id,tags))
                .flatMap(tagsRepository::save).then();
    }

    public Flux<app.model.Posts> getUsersPosts(UUID userId){
        return postsRepository.findByUserId(userId)
                .switchIfEmpty(Mono.error(new ServiceException("Users post not found")));
    }

    public Flux<Tags> getTagsForAPost(String hashtag){
        return postsRepository.findAllTagsOfHashtag(hashtag);
    }

    public Mono<PostsDTO> getDummyPost(String postId){
        return postsRepository.getAllPostsWithTags(UUID.fromString(postId));
    }

    public Flux<app.model.Posts> getPostOfTaggedUsers(UUID taggedId){
           return tagsRepository.findByTaggedTo(taggedId)
                    .map(Tags::getPostsId)
                   .flatMap(postsRepository::findById);
    }

    public Mono<app.model.Posts> getPostByHashtag(String hashtag){
        return postsRepository.findByHashtag(hashtag)
                .switchIfEmpty(Mono.error(new ServiceException("post not found")));

    }

    private Flux<Tags> getAllTags(UUID postId,UUID... tags){
        if (Objects.isNull(tags)){
            return Flux.empty();
        }
        List<Tags> allTags = new ArrayList<>();
        for(UUID taggedId : tags) {
            allTags.add(Tags.builder().taggedTo(taggedId).postsId(postId).build());
        }
        return Flux.fromIterable(allTags);
    }

    public Mono<UUID> getUUIDFromString(String id) {
        return Mono.just(id)
                .flatMap(uuid -> {
                            try {
                                return Mono.just(UUID.fromString(uuid));
                            } catch (Exception err) {
                                return Mono.error(new IllegalArgumentException("Bad UUID String "+id));
                            }
                        }
                );
    }

}
