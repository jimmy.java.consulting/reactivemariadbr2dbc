package app.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CService implements ApplicationContextAware {

    private  ApplicationContext ctx;



    @PostConstruct
    public void init(){
        String[] allBeanNames = ctx.getBeanDefinitionNames();
        for(String name:allBeanNames){
         //   System.out.println(name);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
