package app.repository.callback;

import app.model.base.BaseUUIDEntity;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.data.r2dbc.mapping.event.AfterSaveCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class EntityAfterSaveCallback implements AfterSaveCallback<BaseUUIDEntity> {

    @Override
    public Publisher<BaseUUIDEntity> onAfterSave(BaseUUIDEntity entity, OutboundRow outboundRow, SqlIdentifier table) {
        System.out.println("called ---"+entity.getId());
        return Mono.just(entity);
    }
}
