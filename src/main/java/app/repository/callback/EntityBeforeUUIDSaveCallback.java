package app.repository.callback;

import app.model.base.BaseUUIDEntity;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class EntityBeforeUUIDSaveCallback implements BeforeConvertCallback<BaseUUIDEntity> {
    @Override
    public Publisher<BaseUUIDEntity> onBeforeConvert(BaseUUIDEntity entity, SqlIdentifier table) {
        if (Objects.isNull(entity.getId())) {
            entity.setId(UUID.randomUUID());
            entity.setCreatedDt(LocalDateTime.now());
            entity.setCreatedBy("system-create");
        } else {
            entity.setUpdatedBy("system-update");
            entity.setUpdatedDt(LocalDateTime.now());
        }
        return Mono.just(entity);
    }
}
