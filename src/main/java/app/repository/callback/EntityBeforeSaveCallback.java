package app.repository.callback;

import app.model.base.BaseNumericEntity;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class EntityBeforeSaveCallback implements BeforeConvertCallback<BaseNumericEntity> {

    @Override
    public Publisher<BaseNumericEntity> onBeforeConvert(BaseNumericEntity entity, SqlIdentifier table) {
        if (Objects.isNull(entity.getId())) {
            entity.setCreatedBy("system");
            entity.setCreatedDate(LocalDateTime.now());
        }
        entity.setModifiedBy("system");
        entity.setModifiedDate(LocalDateTime.now());
        return Mono.just(entity);
    }
}
