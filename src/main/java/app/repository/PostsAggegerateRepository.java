package app.repository;

import app.api.dto.PostsDTO;
import java.util.UUID;
import reactor.core.publisher.Mono;

public interface PostsAggegerateRepository {

    Mono<PostsDTO> getAllPostsWithTags(UUID postId);
}
