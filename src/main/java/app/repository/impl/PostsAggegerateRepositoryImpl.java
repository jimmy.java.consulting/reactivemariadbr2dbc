package app.repository.impl;

import app.api.dto.PostsDTO;
import app.model.PostVO;
import app.repository.PostsAggegerateRepository;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Slf4j
public class PostsAggegerateRepositoryImpl implements PostsAggegerateRepository {

    private final DatabaseClient client;
    private final String sql = """
            select p.id, p.post_text,t.tagged_to
            from post.postsDTO p
            LEFT JOIN post.tags t ON p.id = t.posts_id
            where p.id = $1
            """;
    @Transactional
    @Override
    public Mono<PostsDTO> getAllPostsWithTags(UUID postId) {
        log.info(" started call to one-to-many rel wuth id "+postId);
        PostsDTO postsDTO = new PostsDTO();

        return client.inConnection(conn -> {
            return Mono.from(conn.createStatement(sql)
                            .bind("$1", postId)
                            .execute())
                    .flatMapMany(res -> res.map((row, metadata) -> {
                        PostVO vo = PostVO.builder().postText((String) row.get("post_text")).build();
                        if (Objects.nonNull(row.get("tagged_to"))){
                            vo.setTaggedTo(row.get("tagged_to").toString());
                        }
                        return vo;
                    }))
                    .reduce(PostVO.builder().build(), (p1, p2) -> getPostVO(postsDTO, p1, p2))
                    .thenReturn(postsDTO);
        });
    }

    private static PostVO getPostVO(PostsDTO postsDTO, PostVO p1, PostVO p2) {
        if (Objects.isNull(p1.getPostText())) {
            postsDTO.setUserId(p2.getUserId());
            postsDTO.setPostText(p2.getPostText());
            postsDTO.setHashtag(p2.getHashtag());
            postsDTO.setDislikes(p2.getDislikes());
            postsDTO.setLikes(p2.getLikes());
            postsDTO.setTaggedTo(new ArrayList<>());
            postsDTO.getTaggedTo().add(p2.getTaggedTo());
        } else {
            postsDTO.getTaggedTo().add(p2.getTaggedTo());
        }
        return p2;
    }
}
