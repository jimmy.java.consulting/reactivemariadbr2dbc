package app.repository;

import app.model.Posts;
import app.model.Tags;
import app.model.User;
import java.util.UUID;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PostsRepository extends ReactiveCrudRepository<Posts, UUID> , PostsAggegerateRepository {
    Flux<Posts> findByUserId(UUID userId);

    Mono<Posts> findByHashtag(String hashtag);

    @Query("select t.tagged_to , t.posts_id from post.tags t RIGHT JOIN post.posts p ON t.posts_id = p.id where p.hashtag = :hTag")
    Flux<Tags> findAllTagsOfHashtag(String hTag);


}
