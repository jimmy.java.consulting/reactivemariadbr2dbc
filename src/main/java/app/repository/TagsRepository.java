package app.repository;

import app.model.Tags;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TagsRepository extends ReactiveCrudRepository<Tags, UUID> {

    Flux<Tags> findByTaggedTo(UUID taggedTo);
}
